__author__ = 'pauful'

import pickle
import csv
import random as rnd
import string
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from nltk.stem.snowball import EnglishStemmer
from nltk.corpus import stopwords

stemmer = EnglishStemmer(ignore_stopwords=True)
data = []
dataTrain = []
dataTest = []
newData = []
cachedStopWords = stopwords.words("english")
print(cachedStopWords)
vectorizer = CountVectorizer(min_df=1, ngram_range=(1, 2), stop_words=cachedStopWords)
transformer = TfidfTransformer()

def getTfIdfTrain(train, vectorizer, transformer):
    X = vectorizer.fit_transform(train)
    return transformer.fit_transform(X)


def getTfIdfTest(test, vectorizer, transformer):
    
    X = vectorizer.transform(test)
    return transformer.transform(X)

def stem(line):
    newText = ''

    for w in line.split(' '):
        newText += " " + stemmer.stem(w)

    return newText;

def saveFile(obj, fileName):
    fileObject = open(fileName,'wb')
    pickle.dump(obj,fileObject)
    fileObject.close()

def readFile(fileName):
    newData = []
    with open(fileName, 'rb') as csvfile:
        avitoReader = csv.reader(csvfile, delimiter='\t')

        i = 0
        for row in avitoReader:
            #print avitoReader.line_num
            if i == 0 :
                print 'Line 0'
                i = 1
            else:
                newData.append(stem(row[2]))
                phrase = {
                    'phraseId' : row[0],
                    'sentenceId' : row[1],
                    'phrase' : row[2],
                    'sentiment' : row[3]
                }
                data.append(phrase)

    # print newData
    print 'tfidf'
    tfidf = getTfIdfTrain(newData, vectorizer, transformer)
    print 'store tfidf'
    saveFile(tfidf, 'arrayPhrases.pickle')
    print 'saveData'
    saveFile(data, 'arrayData.pickle')


def readFileTest(fileName):

    with open(fileName, 'rb') as csvfile:
        avitoReader = csv.reader(csvfile, delimiter='\t')

        i = 0
        for row in avitoReader:
            #print avitoReader.line_num
            if i == 0 :
                print 'Line 0'
                i = 1
            else:
                newData.append(stem(row[2]))
                phrase = {
                    'phraseId' : row[0],
                    'sentenceId' : row[1],
                    'phrase' : row[2],
                }
                data.append(phrase)

    # print newData
    # print 'tfidf'
    # tfidf = getTfIdfTrain(newData, vectorizer, transformer)
    # print 'store tfidf'
    # saveFile(tfidf, 'arrayPhrasesTest.pickle')
    # print 'saveData'
    saveFile(data, 'arrayDataTestCheck.pickle')


def readFileTrain(fileName):
    newDataTrain = []
    newDataTest = []
    nn = 156061 * 90 /100
    with open(fileName, 'rb') as csvfile:
        avitoReader = csv.reader(csvfile, delimiter='\t')

        i = 0
        for row in avitoReader:
            #print avitoReader.line_num
            if i == 0 :
                print 'Line 0'
                i = 1
            else:
                if i < nn :
                    newDataTrain.append(stem(row[2]))
                    phrase = {
                        'phraseId' : row[0],
                        'sentenceId' : row[1],
                        'phrase' : row[2],
                        'sentiment' : row[3]
                    }
                    dataTrain.append(phrase)
                else:
                    newDataTest.append(stem(row[2]))
                    phrase = {
                        'phraseId' : row[0],
                        'sentenceId' : row[1],
                        'phrase' : row[2],
                        'sentiment' : row[3]
                    }
                    dataTest.append(phrase)

                i += 1

    # print newData

    readFileTest('test.tsv')

    print 'tfidf'
    tfidf = getTfIdfTrain(newDataTrain, vectorizer, transformer)
    print 'store tfidf'
    saveFile(tfidf, 'arrayPhrasesTrain2.pickle')
    tfidf = getTfIdfTest(newData, vectorizer, transformer)
    saveFile(tfidf, 'arrayPhrasesTestTest2.pickle')
    tfidf = getTfIdfTest(newDataTest, vectorizer, transformer)
    print 'store tfidf'
    saveFile(tfidf, 'arrayPhrasesTest2.pickle')
    print 'saveData'
    saveFile(dataTrain, 'arrayDataTrain2.pickle')
    print 'saveData'
    saveFile(dataTest, 'arrayDataTest2.pickle')

def readItems(fileName):
    items = []
    with open(fileName, 'rb') as csvfile:
        avitoReader = csv.reader(csvfile, delimiter='\t')

        i = 0
        for row in avitoReader:
            #print avitoReader.line_num
            if i == 0 :
                print 'Line 0'
                i = 1
            else:
                items.append(row[0])

    saveFile(items, 'itemsTest.pickle')


if __name__=="__main__":
    #readFile('train.tsv')
    readFileTest('test.tsv')
    #readFileTrain('train.tsv')
    #readItems('test.tsv')