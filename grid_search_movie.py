__author__ = 'pauful'

from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier
import pickle

tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000]},
                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]

tuned_parameters_SGD = []

scores = ['precision', 'recall']

def readFile(file):
    f = open(file)
    obj = pickle.load(f)
    f.close()

    return obj

def getArrayOfClasses(data):
    arr = []
    a = 0
    for i in data:
        #print i
        #print 'pauuu'
        #print data[i]
        arr.append(i['sentiment'])

    return arr

for score in scores:
    print("# Tuning hyper-parameters for %s" % score)
    print()
    clf = GridSearchCV(SVC(C=1), tuned_parameters, cv=5, scoring=score)
    clf.fit(readFile('arrayPhrases.pickle'), getArrayOfClasses(readFile('arrayData.pickle')))

    print("Best parameters set found on development set:")
    print()
    print(clf.best_estimator_)
    print()
    print("Grid scores on development set:")
    print()
    for params, mean_score, scores in clf.grid_scores_:
        print("%0.3f (+/-%0.03f) for %r"
              % (mean_score, scores.std() / 2, params))
    print()

    # print("Detailed classification report:")
    # print()
    # print("The model is trained on the full development set.")
    # print("The scores are computed on the full evaluation set.")
    # print()
    # y_true, y_pred = getArrayOfClasses(readFile('arrayData.pickle')), clf.predict(readFile('testTitle5.pickle'))
    # print(classification_report(y_true, y_pred))
    # print()