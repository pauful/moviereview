__author__ = 'pauful'

import pickle

def readFile(lablesFile):
    f = open(lablesFile)
    labels = pickle.load(f)
    f.close()
    return labels
