__author__ = 'pauful'

from pybrain.structure import RecurrentNetwork
from pybrain.structure.modules import SoftmaxLayer
from pybrain.structure.modules import LinearLayer
from pybrain.structure.modules import TanhLayer
from pybrain.structure.connections import FullConnection
from pybrain.datasets import SequentialDataSet
from pybrain.supervised.trainers import BackpropTrainer
import numpy as np
import pickle

n = RecurrentNetwork()

n.addInputModule(LinearLayer(200, name='in'))
n.addModule(TanhLayer(100, name='hidden'))
n.addOutputModule(SoftmaxLayer(5, name='out'))
n.addConnection(FullConnection(n['in'], n['hidden'], name='c1'))
n.addConnection(FullConnection(n['hidden'], n['out'], name='c2'))

n.addRecurrentConnection(FullConnection(n['hidden'], n['hidden'], name='c3'))

def getBinary(num):
    if num == '0':
        return np.asarray([1,0,0,0,0])
    elif num == '1':
        return np.asarray([0,1,0,0,0])
    elif num == '2':
        return np.asarray([0,0,1,0,0])
    elif num == '3':
        return np.asarray([0,0,0,1,0])
    elif num == '4':
        return np.asarray([0,0,0,0,1])

def readFile(file):
    f = open(file)
    obj = pickle.load(f)
    f.close()

    return obj

training = readFile('trainTreeArrWithVecs.pickle')
ds = SequentialDataSet(200, 5)

for train in training['5988']:
    #print train['node']['phrase']
#     print train['second']
#     print getBinary(train['node']['sentiment'])
#     ds.newSequence()
#     ds.appendLinked(tuple(train['first']), tuple(getBinary(train['node']['sentiment'])))
    if type(train['second']) != int and type(train['first']) != int:
        print 'train1'
        print train['node']['phrase']

        ds.newSequence()

        ds.appendLinked(tuple(train['first']), tuple(getBinary(train['node']['sentiment'])))
        ds.appendLinked(tuple(train['second']), tuple(getBinary(train['node']['sentiment'])))
    elif type(train['second']) != int and type(train['first']) == int:
        print 'train2'
        print train['node']['phrase']
        ds.newSequence()
        ds.appendLinked(tuple(train['second']), tuple(getBinary(train['node']['sentiment'])))
    elif type(train['second']) == int and type(train['first']) != int:
        print 'train3'
        print train['node']['phrase']

        ds.newSequence()
        ds.appendLinked(tuple(train['first']), tuple(getBinary(train['node']['sentiment'])))


# ds.newSequence()
# ds.appendLinked(tuple(training['5988'][0]['first']), tuple(getBinary(training['5988'][0]['node']['sentiment'])))
# ds.newSequence()
# ds.appendLinked(tuple(training['5988'][1]['second']), tuple(getBinary(training['5988'][1]['node']['sentiment'])))

#print ds.currentSeq
#ds.append()
#if type(training['5988'][0]['second']) != int :
#    print 'kkkkkkkkkk'
#print training['5988'][0]
#print training['5988'][1]['node']['sentiment']
#n.randomize()
n.sortModules()
trainer = BackpropTrainer(n, ds)

print 'Num seqs = ' + str(ds.getNumSequences())
from numpy import reshape
for _ in range(100):
    trainer.train()
    # print trainer.module
    # print trainer.train()
    # print trainer.module.outputbuffer
    # print 'next'
    #print n.outputbuffer
    #print n.offset
    # print len(n.recurrentConns[0].params)
    # print n.recurrentConns[0].outdim
    # print n.recurrentConns[0].indim
    # print n.connections
    # i = 0
    # for con in n.connections:
    #     if i == 2:
    #         break
    #     print n.connections[con][0]
    #     print n.connections[con][0].outdim
    #     print n.connections[con][0].indim
    #     print n.connections[con][0].params
    #     print len(n.connections[con][0].params)
    #     print 'outputbuffer'
    #     print len(n.connections[con][0].inmod.outputbuffer[0])
    #     print n.connections[con][0].inmod.outputbuffer[0]
    #     print len(n.connections[con][0].outmod.inputbuffer[0])
    #     print n.connections[con][0].outmod.inputbuffer[0]
    #
    #     i += 1
    # print reshape(n.recurrentConns[0].params, (n.recurrentConns[0].outdim, n.recurrentConns[0].indim)).shape
    # print len(n.recurrentConns[0].params)

print training['5988'][0]['node']['phrase']
print n.activate(tuple(training['5988'][0]['first']))
print n.activate(tuple(training['5988'][0]['second']))
n.reset()
print training['5988'][1]['node']['phrase']
print n.activate(tuple(training['5988'][1]['second']))