__author__ = 'pauful'

from pybrain.structure import RecurrentNetwork
from pybrain.structure.modules import SoftmaxLayer
from pybrain.structure.modules import LinearLayer
from pybrain.structure.modules import TanhLayer
from pybrain.structure.connections import FullConnection
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
import pickle
from gensim.models import word2vec
import re

n = RecurrentNetwork()

n.addInputModule(LinearLayer(200, name='in'))
n.addModule(TanhLayer(100, name='hidden'))
n.addOutputModule(SoftmaxLayer(5, name='out'))
n.addConnection(FullConnection(n['in'], n['hidden'], name='c1'))
n.addConnection(FullConnection(n['hidden'], n['out'], name='c2'))


ds = SupervisedDataSet(200, 5)


model = word2vec.Word2Vec.load('word2vec/text8.model')

def toBin(num):
    num = [int(x) for x in list('{0:0b}'.format(num))]
    if len(num) < 5 :
        num = num + [0 for x in range(0, (5 -len(num)))]
    return num

def train() :
    training = pickle.load('word2vec/trainingData.pickle')
    for row in training:
        for word in re.findall(r"[\w']+|[.,!?;]", row['phrase']):
            if (model.__contains__(word)):
                ds.append(model.__getitem__(word), toBin(row))

n.randomize()

trainer = BackpropTrainer(n, ds)

for _ in range(1000):
    print trainer.train()
