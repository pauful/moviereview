__author__ = 'pauful'

import numpy as np

r=np.random.randint(3, size=(1,2000))

from pybrain.datasets.classification import ClassificationDataSet
# below line can be replaced with the algorithm of choice e.g.
# from pybrain.optimization.hillclimber import HillClimber
from pybrain.optimization.populationbased.ga import GA
from pybrain.tools.shortcuts import buildNetwork
import pickle
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from nltk.stem.snowball import EnglishStemmer
from nltk.corpus import stopwords

stemmer = EnglishStemmer(ignore_stopwords=True)
cachedStopWords = stopwords.words("english")
vectorizer = CountVectorizer(min_df=1, max_features=2000, stop_words=cachedStopWords)
transformer = TfidfTransformer()

def stem(line):
    newText = ''
    for w in line.split(' '):
        newText += " " + stemmer.stem(w)

    return newText;

def readFile(file):
    f = open(file)
    obj = pickle.load(f)
    f.close()

    return obj

def getArrayOfClasses(data):
    arr = []
    for i in data:
        arr.append(i['sentiment'])

    return arr


data = readFile('../arrayData.pickle')

classArr = getArrayOfClasses(readFile('../arrayData.pickle'))

daArr = []
for aa in data :
    daArr.append(aa['phrase'])

def getTfIdfTrain(train, vectorizer, transformer):
    X = vectorizer.fit_transform(train)
    return X

dArr = getTfIdfTrain(daArr,vectorizer, transformer)
print vectorizer.get_params()
dArr = dArr.toarray()
print len(dArr)
#DS = ClassificationDataSet(2000, class_labels=['negative', 'somewhat negative', 'neutral', 'somewhat positive', 'positive'])
from pybrain.datasets import SupervisedDataSet
DS = SupervisedDataSet( 2000, 5 )
print 'add samples'
for i in range(0,1000):
    a = ['0']*5
    a[int(classArr[i])] = '1'
    print a
    DS.appendLinked(dArr[i],a)
    # d.addSample(dArr[i], [classArr[i]])
print 'build net'
print classArr[:1000]
nn = buildNetwork(2000, 2000, 5)
print 'learn'
# print d.outdim
# print nn.outdim
#ga = GA(d.evaluateModuleMSE, nn, minimize=True)
#for i in range(100):
#    nn = ga.learn(0)[0]

from pybrain.supervised.trainers import BackpropTrainer

bt =BackpropTrainer(nn, dataset=DS, learningrate=0.01, lrdecay=1.0, momentum=0.0, verbose=False, batchlearning=False, weightdecay=0.0)

print 'train'
error = bt.trainEpochs(epochs=100)
# error = bt.train()
#print error
# bt.trainUntilConvergence()
print 'done'

print nn.activate(dArr[1001])
print daArr[1001]

print nn.activate(dArr[1002])
print daArr[1002]

print nn.activate(dArr[2001])
print daArr[2001]

print nn.activate(dArr[2002])
print daArr[2002]

print nn.activate(dArr[3001])
print daArr[3001]
#print nn.activate(np.random.randint(3, size=(2000, 1)))
# import random
# print nn.activate([random.randrange(0,3) for e in range(2000)])