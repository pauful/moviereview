__author__ = 'pauful'

import re
import pickle

TAG_RE = re.compile(r'<[^>]+>')
INIT_RE = re.compile(r'review/text: ')

fname = '/home/pauful/Downloads/movies.txt'
sentences = []
i = 0
with open(fname) as f:
    for line in f:
        if 'review/text: ' in line:

            #line.replace('review/text: ', '')
            line = INIT_RE.sub('', line)
            line = TAG_RE.sub('', line)
            #print line
            print line
            line = unicode(line, errors='ignore')
            sentences.append(line)
        if i > 200000 :
            break
        i += 1

fileObject = open('moviesSentences.pickle','wb')
pickle.dump(sentences,fileObject)
fileObject.close()

