__author__ = 'pauful'

from gensim.models import word2vec
import pickle

model = word2vec.Word2Vec.load_word2vec_format('text8.model.bin', binary=True)


f = open('trainTreeArr.pickle')
obj = pickle.load(f)
f.close()
newTrain = {}

for sent in obj:
    print sent
    #print obj[sent]
    trainArr = []

    for train in obj[sent]:
        #print train
        print train['first']
        tt =train
        if type(train['first']) == str :
            w = train['first'].replace(',','').strip()
            try:
                tt['first'] = model[w]
            except KeyError:
                tt['second'] = model['the']
                #print 'error: ' + w
        elif type(train['first']) == int and train['first'] == -1 :
            tt['first'] = model['the']

        if type(train['second']) == str :
            w = train['second'].replace(',','').strip()
            try:
                tt['second'] = model[w]
            except KeyError:
                tt['second'] = model['the']
                #print 'error: ' + w
        elif type(train['second']) == int and train['second'] == -1 :
            tt['second'] = model['the']

        trainArr.append(tt)
    newTrain[sent]=trainArr
    print 'aaa'
    break

def saveFile(obj, fileName):
    fileObject = open(fileName,'wb')
    pickle.dump(obj,fileObject)
    fileObject.close()

saveFile(newTrain, 'trainTreeArrWithVecs.pickle')

print 'DONE'