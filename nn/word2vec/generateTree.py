__author__ = 'pauful'

import csv
import Queue
import pickle

data = []
dictio = {}

def extractRestText(str1, str2):
    res = str1.replace(str2, '')
    return res

with open('train.tsv', 'rb') as csvfile:
    avitoReader = csv.reader(csvfile, delimiter='\t')

    i = 0
    for row in avitoReader:
        #print avitoReader.line_num
        if i == 0 :
            print 'Line 0'
            i = 1
        else:
            phrase = {
                'phraseId' : row[0],
                'sentenceId' : row[1],
                'phrase' : row[2],
                'sentiment' : row[3],
            }

            if row[1] not in dictio :
                dictio[str(row[1])] = []
            dictio[str(row[1])].append(phrase)


def generateNode(first, second, node, id):
    #print 'ff '+str(first)
    #print 'ss '+str(second)
    return {
        'first': first,
        'second': second,
        'node': node,
        'id': id
    }


trainingData = {}
#q = Queue.LifoQueue()
i=0


for listId in dictio:
    trainArr = []
    q = Queue.LifoQueue()
    ord = 0

    listSentence = dictio[listId]
    sList = sorted(listSentence, key=lambda k: int(k['phraseId']), reverse=True)
    #print sList
    for phrase in sList:
        #print '>>>'+str(ord)+'<<<'
        #print phrase
        tt = {}
        if q.qsize() == 0:
            #print '0-- '+phrase['phrase']

            phraseArr = phrase['phrase'].split(" ")
            if len(phraseArr) == 1:
                tt= generateNode(-1 ,phraseArr[0],phrase, ord)
            else:
                tt = generateNode(phraseArr[0], phraseArr[1], phrase, ord)
            q.put(tt)
        elif q.qsize() == 1:
            #print '1-- '+phrase['phrase']

            prev = q.get()
            if prev['node']['phrase'] in phrase['phrase']:
                newStr = phrase['phrase'].replace(prev['node']['phrase'], '')
                if phrase['phrase'].index(prev['node']['phrase']) == 0:
                    tt = generateNode(newStr, prev['id'], phrase, ord)
                else:
                    tt = generateNode( prev['id'],newStr, phrase, ord)
                q.put(tt)
            else:
                phraseArr = phrase['phrase'].strip().split(" ")
                if len(phraseArr) == 1:
                    tt= generateNode(-1 ,phraseArr[0],phrase, ord)
                else:
                    tt = generateNode(phraseArr[0], phraseArr[1], phrase, ord)
                q.put(prev)
                q.put(tt)
        else:
            qsizeArr = []

            prev = q.get()
            if prev['node']['phrase'] in phrase['phrase']:
                prevprev = q.get()
                #print '2-1 '+phrase['phrase'] + " - " +str(prevprev) + " / " + str(prev)
                if phrase['phrase'].index(prev['node']['phrase']) == 0 and prevprev['node']['phrase'] in phrase['phrase']:
                    tt = generateNode(prev['id'], prevprev['id'],phrase, ord)
                elif prevprev['node']['phrase'] in phrase['phrase'] and  phrase['phrase'].index(prevprev['node']['phrase']) == 0 and prev['node']['phrase'] in phrase['phrase']:
                    tt = generateNode(prevprev['id'], prev['id'],phrase, ord)
                else:

                    newStr = phrase['phrase'].replace(prev['node']['phrase'], '').strip()

                    #print '2-1 ' + newStr
                    if phrase['phrase'].index(prev['node']['phrase']) == 0:
                        tt = generateNode(prev['id'], newStr,phrase, ord)
                    else:
                        tt = generateNode(newStr, prev['id'], phrase, ord)
                    q.put(prevprev)
                q.put(tt)
            else :
                prevprev = q.get()

                #print '2-2 ' + " - " + str(prevprev) + " / " + str(prev)
                if prevprev['node']['phrase'] in phrase['phrase']:


                    newStr = phrase['phrase'].replace(prev['node']['phrase'], '').strip()

                    #print '2-2 ' + newStr
                    if phrase['phrase'].index(prevprev['node']['phrase']) == 0:
                        tt = generateNode(prevprev['id'], newStr,phrase, ord)
                    else:
                        tt = generateNode(newStr, prevprev['id'], phrase, ord)
                    q.put(prev)
                    q.put(tt)
                else:

                    #print '2-X '+phrase['phrase']
                    phraseArr = phrase['phrase'].strip().split(" ")
                    if len(phraseArr) == 1:
                        tt= generateNode(-1 ,phraseArr[0],phrase, ord)
                    else:
                        tt = generateNode(phraseArr[0], phraseArr[1], phrase, ord)
                    q.put(prevprev)
                    q.put(tt)

        trainArr.append(tt)
        ord +=1
    trainingData[listId] = trainArr


def saveFile(obj, fileName):
    fileObject = open(fileName,'wb')
    pickle.dump(obj,fileObject)
    fileObject.close()

saveFile(trainingData, 'trainTreeArr.pickle')
print 'DONE'
#print trainingData


