__author__ = 'pauful'

from pybrain.structure import FeedForwardNetwork

n = FeedForwardNetwork()

from pybrain.structure import LinearLayer, SigmoidLayer, TanhLayer
inLayer = LinearLayer(1)
hiddenLayer = TanhLayer(3)
outLayer = LinearLayer(1)
# outLayer2 = LinearLayer(3)

n.addInputModule(inLayer)
n.addModule(hiddenLayer)
n.addOutputModule(outLayer)
# n.addOutputModule(outLayer2)

from pybrain.structure import FullConnection
in_to_hidden = FullConnection(inLayer, hiddenLayer)
hidden_to_out = FullConnection(hiddenLayer, outLayer)
# hidden_to_out2 = FullConnection(hiddenLayer, outLayer2)

n.addConnection(in_to_hidden)
n.addConnection(hidden_to_out)
# n.addConnection(hidden_to_out2)

n.sortModules()

from pybrain.datasets import SupervisedDataSet
DS = SupervisedDataSet(1, 1)

DS.appendLinked([0], [0])
# DS.appendLinked([0], [0])
# DS.appendLinked([1], [0])
DS.appendLinked([1], [1])

# from pybrain.supervised.trainers import BackpropTrainer
#
# trainer = BackpropTrainer(n, DS)
#
# for i in range(1, 100):
#     trainer.train()
#trainer.trainUntilConvergence()

print n.activate([1])