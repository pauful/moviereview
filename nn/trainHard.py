__author__ = 'pauful'

from pybrain.structure import RecurrentNetwork
from pybrain.structure.modules import SoftmaxLayer
from pybrain.structure.modules import LinearLayer
from pybrain.structure.modules import TanhLayer
from pybrain.structure.connections import FullConnection
from pybrain.datasets import SequentialDataSet
from pybrain.supervised.trainers import BackpropTrainer
import numpy as np
from gensim.models import word2vec
import pickle
import csv

n = RecurrentNetwork()

n.addInputModule(LinearLayer(200, name='in'))
n.addModule(TanhLayer(100, name='hidden'))
n.addOutputModule(SoftmaxLayer(5, name='out'))
n.addConnection(FullConnection(n['in'], n['hidden'], name='c1'))
n.addConnection(FullConnection(n['hidden'], n['out'], name='c2'))

n.addRecurrentConnection(FullConnection(n['hidden'], n['hidden'], name='c3'))

def getBinary(num):
    if num == '0':
        return np.asarray([1,0,0,0,0])
    elif num == '1':
        return np.asarray([0,1,0,0,0])
    elif num == '2':
        return np.asarray([0,0,1,0,0])
    elif num == '3':
        return np.asarray([0,0,0,1,0])
    elif num == '4':
        return np.asarray([0,0,0,0,1])

def readFile(file):
    f = open(file)
    obj = pickle.load(f)
    f.close()

    return obj

training = readFile('trainTreeArrWithVecs.pickle')
ds = SequentialDataSet(200, 5)

model = word2vec.Word2Vec.load_word2vec_format('text8.model.bin', binary=True)

with open('train.tsv', 'rb') as csvfile:
    avitoReader = csv.reader(csvfile, delimiter='\t')

    i = 0
    for row in avitoReader:
        #print avitoReader.line_num
        if i == 0 :
            print 'Line 0'
            i = 1
        elif i == 100:
            break
        else:
            phrase = {
                'phraseId' : row[0],
                'sentenceId' : row[1],
                'phrase' : row[2],
                'sentiment' : row[3],
            }
            lW = row[2].split(' ')
            if len(lW) > 0 :

                aw = []
                for w in lW :
                    try:
                        aw.append(model[w])
                    except KeyError:
                        t = 0
                if len(aw) > 0:
                    ds.newSequence()
                    for aa in aw :
                        ds.appendLinked(aa, tuple(getBinary(row[3])))

        i += 1

n.sortModules()
trainer = BackpropTrainer(n, ds)

print 'Num seqs = ' + str(ds.getNumSequences())
from numpy import reshape
#trainer.trainUntilConvergence()
for _ in range(100):
    trainer.train()

# print n.activate(model['goose'])
# n.reset()
# print n.activate(model['for'])
print n.activate(model['independent'])
# n.reset()
# print training['5988'][1]['node']['phrase']
# print n.activate(tuple(training['5988'][1]['second']))