__author__ = 'pauful'

import numpy as np

r=np.random.randint(3, size=(1,2000))

from pybrain.datasets.classification import ClassificationDataSet
# below line can be replaced with the algorithm of choice e.g.
# from pybrain.optimization.hillclimber import HillClimber
from pybrain.optimization.populationbased.ga import GA
from pybrain.tools.shortcuts import buildNetwork

d = ClassificationDataSet(2000)
print 'add samples'
for i in range(0,100):
    d.addSample(np.random.randint(3, size=(1,2000)), [i % 2])
print 'build net'
nn = buildNetwork(2000, 200, 1)
print 'learn'
ga = GA(d.evaluateModuleMSE, nn, minimize=True)
for i in range(100):
    nn = ga.learn(0)[0]
print 'done'

#print nn.activate(np.random.randint(3, size=(2000, 1)))
import random
print nn.activate([random.randrange(0,3) for e in range(2000)])