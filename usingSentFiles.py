__author__ = 'pauful'

import pickle
import csv

def readFile(file):
    f = open(file)
    obj = pickle.load(f)
    f.close()

    return obj


def readPositive(file):
    arra = []
    with open(file, 'rb') as csvfile:
        avitoReader = csv.reader(csvfile, delimiter='\t')

        i = 0
        for row in avitoReader:
            arra.append(row[0])
    return arra

def readSent(testText, pos, neg):
    sentVal = []
    for t in testText :
        val = 0
        for w in t.split(' '):
            if w in pos:
                val += 1
            if w in neg:
                val -= 1
        sentVal.append(str(val))

    return sentVal

def getSentence(data):
    arr =[]
    for i in data:
        arr.append(i['phrase'])
    return arr

def getSent(data):
    arr =[]
    for i in data:
        arr.append(i['sentiment'])
    return arr



def generateDict(calcSents, sents):
    res = {}
    for i in range(0, len(calcSents)):
        if sents[i] not in res :
            res[sents[i]] = {}
        if calcSents[i] not in res[sents[i]] :
            res[sents[i]][calcSents[i]] = 0
        res[sents[i]][calcSents[i]] += 1
    return res

def printSent(calcSents,sents, phrase):
    items = readFile('itemsTest.pickle')

    f = open('results2.txt', "w")
    f.write("PhraseId,Sentiment\n")

    for i in range(0,len(items)):
       f.write("%s,%s,%s, %s,\n" % (i+1,calcSents[i],sents[i],phrase[i]))
    f.close()
    print 'DONE'


def main():
    data1 = readFile('arrayDataTest2.pickle')
    data2 = readFile('arrayDataTrain2.pickle')
    pos = readPositive('positive-words.txt')
    neg = readPositive('negative-words.txt')

    sents = getSent(data1) + getSent(data2)
    print len(sents)
    phrases = getSentence(data1) + getSentence(data2)
    print len(phrases)

    print 'calcsents'
    calcSents = readSent(phrases, pos, neg)
    print 'generatedict'
    print generateDict(calcSents, sents)

    printSent(calcSents, sents,phrases)




if __name__=="__main__":
    main()