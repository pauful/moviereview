__author__ = 'pauful'

import pickle
from nltk.corpus import stopwords

def readFile(file):
    f = open(file)
    obj = pickle.load(f)
    f.close()

    return obj

cachedStopWords = stopwords.words("english")

wordSent = {}
wordWords = {}
wordWordsTest = {}

def readWordsSentimentTrain():
    arr = readFile('arrayData.pickle')
    for a in arr :
        text = a['phrase']
        sent = a['sentiment']
        newText = [x for x in text.split(' ') if x not in cachedStopWords]
        newText = " ".join(newText)
        if(len(newText.split(' ')) == 1):
            w = newText.split(' ')[0]
            if(w != ''):
                wordSent[w] = sent
            #wordSent.append((newText.split(' '),sent))

def wordsInDoc():
    arr = readFile('arrayData.pickle')
    for a in arr :
        text = a['phrase']
        newText = [x for x in text.split(' ') if x not in cachedStopWords]
        for nn in newText :
            wordWords[nn] = 0

def wordsInDocTest():
    arr = readFile('arrayDataTest.pickle')
    for a in arr :
        text = a['phrase']
        newText = [x for x in text.split(' ') if x not in cachedStopWords]
        for nn in newText :
            wordWordsTest[nn] = 0


def test():
    arr = readFile('arrayData.pickle')
    tt = arr[157]['phrase']
    print tt + " -> " + arr[157]['sentiment']
    for a in tt.split(' '):
        if a in wordSent:
            print a + " " + wordSent[a]

readWordsSentimentTrain()
print len(wordSent)
# wordsInDoc()
# print len(wordWords)
# wordsInDocTest()
# print len(wordWordsTest)

# i = 0
# for a in wordWordsTest:
#     if a not in wordSent:
#         i += 1
# print i

print '-------------'

test()
