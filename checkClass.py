__author__ = 'pauful'

from sklearn.linear_model import SGDClassifier
import pickle
from sklearn import cross_validation
from sklearn.grid_search import GridSearchCV
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors.nearest_centroid import NearestCentroid
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LinearRegression
import numpy as np
import scipy
from sklearn.neighbors import DistanceMetric

def readFile(file):
    f = open(file)
    obj = pickle.load(f)
    f.close()

    return obj

def getArrayOfClasses(data):
    arr = []
    tipes = []
    a = 0
    for i in data:
        #print i
        #print 'pauuu'
        #print data[i]
        arr.append(i['sentiment'])
        #if i['sentiment'] not in tipes:
            #tipes.append(i['sentiment'])
            #print i['sentiment']

    return arr


def crossValid():
    clf = SGDClassifier(penalty='l1')
    scores = cross_validation.cross_val_score(clf, readFile('arrayPhrases.pickle'),
                                          getArrayOfClasses(readFile('arrayData.pickle')), cv=5, scoring='f1')
    print scores

def crossValidSVC():
    clf = SVC(C=1.0)
    scores = cross_validation.cross_val_score(clf, readFile('arrayPhrases.pickle'),
                                          getArrayOfClasses(readFile('arrayData.pickle')), cv=5, scoring='f1')
    print scores

def grid(clas, tuned_parameters) :


    #tuned_paramtersSVC = [{'kernel': ['rbf', 'linear']}]

    #scores = ['precision','recall']

    clf = GridSearchCV(clas, tuned_parameters, cv=5)#, scoring=scores)
    #clf = GridSearchCV(SVC(C=1.0), tuned_paramtersSVC, cv=5, scoring=scores)

    clf.fit(readFile('arrayPhrases.pickle'), getArrayOfClasses(readFile('arrayData.pickle')))

    print("Best parameters set found on development set:")
    print()
    print(clf.best_estimator_)
    print()
    print("Grid scores on development set:")
    print()
    for params, mean_score, scores in clf.grid_scores_:
        print("%0.3f (+/-%0.03f) for %r"
              % (mean_score, scores.std() / 2, params))
    print()


def gridSGD() :
    tuned_paramters = [{'penalty': ['l1', 'l2'], 'loss': ['hinge', 'log', 'perceptron'], 'alpha':[0.0001,0.001, 0.01]}]
    grid(SGDClassifier(penalty='l1'), tuned_paramters)

def gridSVC() :
    #tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4], 'C': [1, 10]}]#,
                   # {'kernel': ['linear'], 'C': [1, 10]}]
    #tuned_parameters = [{'kernel': ['rbf']}]#,
    #grid(SVC(1.0), tuned_parameters)

    clf = SVC(C=1.0, kernel='rbf')
    h =getArrayOfClasses(readFile('arrayData.pickle'))
    b = readFile('arrayPhrases.pickle')

    scores = cross_validation.cross_val_score(clf, b, y=h, cv=5)
    print scores

def gridRandomForest():
    tuned_parameters = [{'n_estimators': [10]}]
    grid(RandomForestClassifier(), tuned_parameters)

def gridKmean():
    tuned_parameters = [{'max_iter': [300]}]
    grid(KMeans(n_clusters=5), tuned_parameters)

def linearRegression():
    tuned_parameters = [{'fit_intercept': [True,False]}]
    grid(LinearRegression(), tuned_parameters)

def kmeans():
    clf = KMeans(n_clusters=5)
    h =getArrayOfClasses(readFile('arrayData.pickle'))
    b = readFile('arrayPhrases.pickle')

    scores = cross_validation.cross_val_score(clf, b, y=h, cv=5)#, scoring='f1')
    print scores

def kneighbour():
    clf = KNeighborsClassifier()
    h =getArrayOfClasses(readFile('arrayData.pickle'))
    b = readFile('arrayPhrases.pickle')
    b.toarray()
    scores = cross_validation.cross_val_score(clf, b, y=h, cv=5)#, scoring='f1')
    print scores

def kneighGrid():
    tuned_parameters = [{'p': [1]}]
    grid(KNeighborsClassifier(), tuned_parameters)

def nearestCentroid():
    clf = NearestCentroid()
    h =getArrayOfClasses(readFile('arrayData.pickle'))
    b = readFile('arrayPhrases.pickle')

    scores = cross_validation.cross_val_score(clf, b, y=h, cv=5)
    print scores

def multinomialNB():
    clf = MultinomialNB()
    h =getArrayOfClasses(readFile('arrayData.pickle'))
    b = readFile('arrayPhrases.pickle')

    scores = cross_validation.cross_val_score(clf, b, y=h, cv=5)
    print scores

    tuned_parameters = [{'alpha': [2,1,0.1,0.01]}]
    grid(MultinomialNB(), tuned_parameters)

import csv

def readPositive(file):
    arra = []
    with open(file, 'rb') as csvfile:
        avitoReader = csv.reader(csvfile, delimiter='\t')

        i = 0
        for row in avitoReader:
            arra.append(row[0])
    return arra

def testSentCalc(file):
    testText = readFile(file)
    pos = readPositive('positive-words.txt')
    neg = readPositive('negative-words.txt')
    sentVal = []
    #print testText
    #print pos
    #print neg
    for t in testText :
        val = 0
        for w in t['phrase'].split(' '):
            if w in pos:
                val += 1
            if w in neg:
                val -= 1
        sentVal.append(str(val))

    return sentVal

def generateRandomTreeVals():
    h =getArrayOfClasses(readFile('arrayDataTrain2.pickle'))
    b = readFile('arrayPhrasesTrain2.pickle')

    clf1 = SGDClassifier(alpha=0.0001, class_weight=None, epsilon=0.1, eta0=0.0,
       fit_intercept=True, l1_ratio=0.15, learning_rate='optimal',
       loss='hinge', n_iter=5, n_jobs=1, penalty='l2', power_t=0.5,
       random_state=None, shuffle=False, verbose=0, warm_start=False)

    clf2 = SGDClassifier(alpha=0.0001, class_weight=None, epsilon=0.1, eta0=0.0,
       fit_intercept=True, l1_ratio=0.15, learning_rate='optimal',
       loss='hinge', n_iter=5, n_jobs=1, penalty='l1', power_t=0.5,
       random_state=None, shuffle=False, verbose=0, warm_start=False)

    clf4 = MultinomialNB(alpha=1, class_prior=None, fit_prior=True)

    clf3 = MultinomialNB(alpha=2, class_prior=None, fit_prior=True)

    clf1.fit(b, h)
    clf2.fit(b, h)
    clf3.fit(b, h)
    clf4.fit(b, h)

    b2 = readFile('arrayPhrasesTest2.pickle')
    randomDataTest = clf1.predict(b2)

    predict = clf2.predict(b2)
    randomDataTest = np.vstack([randomDataTest, predict])

    predict = clf3.predict(b2)
    randomDataTest = np.vstack([randomDataTest, predict])

    predict = clf4.predict(b2)
    randomDataTest = np.vstack([randomDataTest, predict])

    predict = testSentCalc('arrayDataTest2.pickle')
#    print np.shape
#    print len(predict)
    #np.asarray(predict)
    randomDataTest = np.vstack([randomDataTest, np.asarray(predict)])


    print randomDataTest


    b3 = readFile('arrayPhrasesTestTest2.pickle')
    randomDataTest2 = clf1.predict(b3)

    predict = clf2.predict(b3)
    randomDataTest2 = np.vstack([randomDataTest2, predict])

    predict = clf3.predict(b3)
    randomDataTest2 = np.vstack([randomDataTest2, predict])

    predict = clf4.predict(b3)
    randomDataTest2 = np.vstack([randomDataTest2, predict])



    predict = testSentCalc('arrayDataTestCheck.pickle')
    print np.shape
    print len(predict)
    randomDataTest2 = np.vstack([randomDataTest2, predict])





    h2 =getArrayOfClasses(readFile('arrayDataTest2.pickle'))
    print h2
    rf = RandomForestClassifier(n_estimators=10)
    rf.fit(randomDataTest.transpose(), h2)

    predictedResult =  rf.predict(randomDataTest2.transpose())

    print len(predictedResult)
    items = readFile('itemsTest.pickle')

    f = open('results2.txt', "w")
    f.write("PhraseId,Sentiment\n")

    for i in range(0,len(items)):
       f.write("%s,%s\n" % (items[i],predictedResult[i]))
    f.close()
    print 'DONE'


#gridSGD()
#gridSVC()
#gridRandomForest()
#gridKmean()
#kmeans()
#nearestCentroid()
#multinomialNB()
generateRandomTreeVals()
#linearRegression()
#kneighGrid()
#kneighbour()